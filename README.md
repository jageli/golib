# golib

## 下载
```
    go get gitlab.com/jageli/golib
```
## 使用
```go
import (
	"fmt"

	"gitlab.com/jageli/golib"
)

func main() {
	fmt.Println("hello")
	aa := golib.Sub(1, 2)
	bb := golib.Add(2, 3)
	fmt.Println(aa)
	fmt.Println(bb)

}

```